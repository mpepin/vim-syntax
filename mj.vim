" Vim syntax file
" Language:       Minijazz
" Filenames:      *.mj

" Minijazz is case sensitive
syntax case match


" Mismatches 
syntax match Error "end where"
syntax match Error "end if"
syntax match Error ")"
syntax match Error "\]"
syntax match Error "\*)"


" Functions
syntax match Function "[a-zA-Z][a-zA-Z0-9_]*[ \t\n]*\(<[^>]\+>\)\@="
syntax match Function "[a-zA-Z][a-zA-Z0-9_]*[ \t\n]*\(([^\*]\)\@="

" Static type parameters
syntax region Type start="<" end=">"
syntax match Type ":\[[^\]]\+\]"

" Builtin functions
syntax keyword Identifier mux and or xor not reg

" Builtin operators and symbols
syntax match Statement "\*"
syntax match Statement "+"
syntax match Statement "&"
syntax match Statement "\^"
syntax match Statement "\."
syntax match Statement ";"
syntax match Statement ","
syntax match Statement "="
syntax match Statement "[<>]="

" Integers
syntax match Number "\<\d\+\>"

" If statements
syntax region None matchgroup=Statement start="\<if\>" matchgroup=Statement end="\<end if\>" contains=ALL
syntax keyword Statement then else

" Block definition
syntax region None matchgroup=Statement start="\<where\>" matchgroup=Statement end="\<end where\>" contains=ALL

" Special highlight for require: not a Minijazz standard but used in our custom
" implementation
syntax match Include "^require"

" Brackets and co
syntax region None matchgroup=Keyword start="(" matchgroup=Keyword end=")" contains=ALL
syntax region None matchgroup=Keyword start="\[" matchgroup=Keyword end="\]" contains=ALL

" Constants
syntax keyword Constant false true
syntax match Constant "\[\]"

" Comments
syntax region mjComment start="(\*" end="\*)" contains=mjTodos,mjComment
syntax keyword mjTodos TODO FIXME XXX

" Custom groups
highlight link mjTodos Todo
highlight link mjComment Comment
