# Vim syntax files

There is actually only one file here yet: for the minijazz programming language

## How to

The recommended way to install those files is:

1. Clone the repository somewhere on your system
2. For each `foo.vim`, create a symlink

    `~/.vim/syntax/foo.vim -> $REPO_FOLDER/foo.vim`

   Create the folder `~/.vim/syntax` if necessary.

3. Activate the syntax option and bind minijazz to the `.mj` extension by adding
   the following two lines in your `.vimrc` (or `~/.vim/vimrc`)

```
syntax on
autocmd BufRead,BufNewFile,BufFilePre *.mj setfiletype mj
```
