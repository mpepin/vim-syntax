" Vim syntax file
" Language:       Net-list
" Filenames:      *.net

" Builtin operators
syntax keyword Identifier SELECT CONCAT SLICE AND OR NOT SELECT RAM REG

" Symbols
syntax match Special ","
syntax match Special "="

" Statements
syntax keyword Statement INPUT OUTPUT VAR IN

" Types
syntax match Type ": \d\+"

" Integres
syntax match Constant "\<\d\+\>"
